<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Contributing to ARPA2CM

Merge requests are welcome! We would like to accept your contributions
to ARPA2CM, and there are several areas where you can help. Non-exhaustively:

- documentation for the modules
- translation of documentation
- the CMake modules themselves


## Join the Conversation

ARPA2 as a whole does not have any public instant-messaging or chat channels.
You can find the authors at other Open-Source projects and channels, for
instance `#calamares` on libera.chat (IRC) or `#calamares:kde.org` (Matrix).
Feel free to ask there and we'll figure something out.

Most project communication goes through the issues-tracker on GitLab.


## Code of Conduct

The ARPA2CM community -- of developers, translators, and users --
aims to be courteous, professional, and inclusive. Harrassment, discriminatory
statements and abuse are not tolerated. In general, we apply the
[KDE Code of Conduct](https://www.kde.org/code-of-conduct/) and the
[GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) (the
rules of decent behavior in both communities are pretty much the same).


## Merge Requests

Please fork ARPA2CM on GitLab and then submit a merge request. It is recommended
to do a merge request **early** and tag it with *Draft* or *WIP* (work-in-progress)
so that we know you are interested in something.

If you submit a merge request, please mention one of the maintainers in the
comments of the merge request so that a notification gets sent out.


## Code Requirements

> When you write code for ARPA2CM, we ask you to abide by the requirements
> of the repository. We choose these requirements so that everyone can
> contribute and everyone can use the modules. There are also conventions
> for the repository. These are also chosen so that we have, as much as
> possible, consistent code throughout the repository.

### LICENSE

The license for the modules is, in general [BSD-2-Clause](LICENSE). Please use that
for your own contributions as well. Do add a copyright notice if applicable. Some
code in the repository is BSD-3-Clause or MIT licensed. If you need a different
license, please make that clear in your merge requests.

ARPA2CM applies the [reuse](https://reuse.software/) tool for license-checking
and uses SPDX tags throughout. With the reuse tool, you can easily check if the
repository with your contributions is properly-tagged with license information:

```
$ reuse lint
.. (output snipped) ..
* Files with copyright information: 52 / 52
* Files with license information: 52 / 52

Congratulations! Your project is compliant with version 3.0 of the REUSE Specification :-)
```


### CMake Style

ARPA2CM has inherited its general CMake style from KDE Frameworks (KDE extra-
cmake-modules in particular), but for overall consistency we apply the
(gersemi)[https://github.com/BlankSpruce/gersemi) formatting tool
to the codebase. There is a `.gersemirc` file with the specific
settings for this project. Use gersemi 0.7.5 or later.

Please run `gersemi` on source-files (CMake modules) before submitting
them. This ensures that all of the modules have the same overall style.
Feel free to apply other styles while doing your own development, as long
as the code that ends up in the repository is consistent.

As general guidelines (the formatting tool will **not** do this for you):
- function calls are *lowercase*
- variables are *uppercase* except
- local variables in macros and functions are *lowercase* (and often start with `_`)
