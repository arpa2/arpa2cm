<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Build and Install Instructions

> These are instructions for building and installing ARPA2 components
> by hand. If your operating system has packages available for the
> components, you are advised to use those.

## In A Nutshell

Like all ARPA2 (sub-)projects, ARPA2CM has a three-stage build:
 - run `cmake` with suitable arguments
 - run the build-system that is generated
 - run the install step that is generated

For Windows, this is probably different (e.g. all contained within Visual
Studio. On UNIX-like systems, to spell it out,

```
mkdir build
cd build
cmake ..
make
make install
```

This creates a separate build directory (inside the source checkout; you
can also do it somewhere else entirely), runs `cmake` telling it where
the sources are. The default generator for CMake is Makefiles, so
then `make` is the tool to do the build and install steps.


## Dependencies

ARPA2CM uses [CMake](https://cmake.org/) to build; you will need at least
version 3.18.


## Options

ARPA2CM has no options.

The software -- mostly CMake modules -- is installed in the prefix defined
by CMake. Use `-DCMAKE_INSTALL_PREFIX` at cmake-time to install to a
different prefix.
