<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# ARPA2CM

> CMake Module library for the ARPA2 project, like KDE Extra CMake Modules (ECM)

The CMake module library for the ARPA2 project, including the LillyDAP,
TLSPool and IdentityHub software stacks. Like the KDE Extra CMake Modules (ECM)
which is a large-ish collection of curated CMake modules of particular
interest to Qt-based and KDE Frameworks-based applications, the ARPA2
CMake Modules (ARPA2CM) is a collection of modules for the software
stack from the ARPA2 project. This is largely oriented towards
TLS, SSL, X509, DER and LDAP technologies. The ARPA2 CMake Modules
also include modules used for product release and deployment of
the ARPA2 software stack.

Some modules in the ARPA2 CM are copied from KDE ECM; this is so
that ARPA2 software only need rely on the ARPA2 CM, instead of
on KDE ECM as well. These duplicates are documented later.

For history information, see `CHANGES`.

## Using ARPA2CM

When ARPA2 CMake Modules are installed, use `find_package()` to find the
modules. This sets one variable, `ARPA2CM_MODULE_PATH`, which should be
added to your `CMAKE_MODULE_PATH`. Good-practice will look for the
module and bail out with a polite error if it is not found. There is an
[example CMake snippet](CMakeLists.example.txt), or the
quick-and-dirty approach to using ARPA2CM is this:

```
find_package (ARPA2CM QUIET NO_MODULE REQUIRED)
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ARPA2CM_MODULE_PATH})
```

Once the module is found, the ARPA2 CMake modules listed below can be used
with `include(<module>)` or `find_package(<module>)`.

### Find Modules

Each of these modules defines `_LIBRARIES` and `_INCLUDE_DIRECTORIES`
variables prefixed by the (case-sensitive) name of the module, and also
defines a `_FOUND` variable which may be false if the requested software
cannot be found.

Modules labeled *modern* provide one or more imported targets and
follow modern CMake variable conventions, prefixing the names of
variables with case-matched versions of the module name.

 - *BDB* finds the Berkeley database, version 5.  (*modern*)
 - *Cyrus-SASL2* finds the SASL2 libraries.  (*modern*)
 - *GnuTLSDane* checks if the installed GnuTLS module has the DANE extensions
   proposed by the ARPA2 project, which are being upstreamed. This module
   defines only a `GnuTLSDane_FOUND` variable, indicating the presence of the
   extensions.  (*modern*)
 - *GPerf* looks for GNU Perf, the perfect-hash generating tool. This module
   is copied from KDE ECM.
 - *KERBEROS* finds any Kerberos implementation (MIT or Heimdal). Since the
   Kerberos implementations are incompatible in **some** API, it is better to
   use the MIT-specific module.  (*modern*)
 - *LibEv* looks for the corresponding library.  (*modern*)
 - *LibTASN1* looks for the corresponding library.  (*modern*)
 - *Libldns* looks for the corresponding library.  (*modern*, namespace `DNS::`)
 - *Log4cpp* looks for the corresponding library.  (*modern*)
 - *MIT-krb5* looks for MIT Kerberos specifically.  (*modern*, namespace `kerberos::`)
 - *OpenLDAP* looks for the corresponding library. It also looks for the BER
   library from OpenLDAP, and includes that in the `_LIBRARIES` variable.
   (*modern*)
 - *P11-Kit* looks for the corresponding library.  (*modern*)
 - *RAGEL* looks for the Ragel tools for state-machines. This module is
   a fork of external work by Georg Sauthoff.
 - *Unbound* looks for the libraries for the caching resolving DNS server
   from NLNet labs.  (*modern*, namespace `DNS::`)
 - *com_err* looks for the corresponding library and tools and provides
   support macros for working with error tables.  (*modern*)
 - *freeDiameter* locates freeDiameter libraries and version information. (*modern*)
 - *lmdb* looks for the corresponding librrary.  (*modern*)

One more CMake file, *ECMFindModuleHelpers*, is included as support for *FindGperf*,
which comes from KDE ECM.

### ARPA2 Modules

ARPA2 projects have certain standards for building the ARPA2 software stack
itself. These are supported by the modules in the `modules/` directory.
They may be useful for other projects as well; they duplicate functionality
found in KDE ECM and elsewhere.

 - *MacroAddUninstallTarget* is boilerplate code from the CMake wiki,
   adding an `uninstall` target to the build.
 - *MacroCreateConfigFiles* creates standard config-files for the current
   project. These are the `Config.cmake`, `ConfigVersion.cmake` and `.pc`
   files that can be used by CMake and pkg-config to find the current
   project once it is installed.
 - *MacroEnsureOutOfSourceBuild* complains when building in-source; most
   ARPA2 software has a top-level `Makefile` that creates a separate build-
   directory and runs CMake there. This macro helps ensure that we don't
   accidentally build in the source tree and pollute it with build artifacts.
 - *MacroGitVersionInfo* uses `git describe` to assign a version number
   if there is a git checkout available; ARPA2 projects should still contain
   version information for non-git source distribution. An ARPA2 project
   should use the `project()` command with versioning information, and
   call `get_version_from_git()` later for tweak-support.
 - *MacroLibraryPair* helps produce pairs of libraries -- one static,
   one shared -- from the same sources.
 - *PackAllPossible* queries the build system to find out which packaging
   mechanisms are available, and configures CPack to use all of them.
 - *PythonSupport* provides some simplistic helper functions for working
   with Python 3, which is the preferred Python version for use with ARPA2.
 - *UseInstallRPATH* sets up RPATH suitably for the installation directory
   that is used. Include this module just once.


## Building ARPA2CM

See `INSTALL.md` .


## Contributing to ARPA2CM

Merge requests are welcome! See `CONTRIBUTING.md` .


## LICENSE

The [BSD 2-clause license](LICENSE) applies to the collection of ARPA2CM
modules. Individual files may be licensed differently -- see LICENSES/
for specifics, and the SPDX identifiers in individual files.
