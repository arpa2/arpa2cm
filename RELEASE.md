<!-- SPDX-FileCopyrightText: no
     SPDX-License-Identifier: CC0-1.0
-->

# Releases

This documents how-to-do-an-ARPA2CM-release. The generic instructions
in ARPA2/howto apply, and only additional steps are documented here.

> For a list of ARPA2CM releases, see the `CHANGES` file.


## Version Tagging

- ARPA2CM releases after each bugfix (or collection of bugfixes that
  are all done in a short period of time).
- ARPA2CM releases when a module is added.
- ARPA2CM uses full major.minor.patch versioning, even for patch-level 0.


## Packaging Information

There are packaging templates in the `contrib/` directory, which can
be updated **after** a release to include information about that
release. So once a release is done, typically a first "next step"
is updating the package templates.

- *FreeBSD* packaging must be updated **after** a release, because it
  will download a release tarball from GitLab and calculate checksums on it.
  - Update the `DISTVERSION` value in the `Makefile` to the new version.
  - Run `make makesum`. If you have a standard ports-tree checked out at
    `/usr/ports` this will work. Otherwise you will need to set `PORTSDIR`
    on the command-line as well.
  - Check that the port builds and installs with `make stage-qa` . The
    same comment about `PORTSDIR` applies.
  - Commit the update to the FreeBSD packaging information.
