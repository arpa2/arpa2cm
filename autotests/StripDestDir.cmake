# Strip a DESTDIR from a path
#
# Tests / examples.

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2021, Rick van Rein <rick@openfortress.nl>
#

include(../modules/MacroStripDestDir.cmake)

# Test programs
#
macro(simple_test VAR_VALUE DESTDIR_VALUE)
    set(A "${VAR_VALUE}")
    stripdestdir(A "${DESTDIR_VALUE}")
    if(A STREQUAL "/tree/yz")
        message("A = '${A}' --> should be '/tree/yz'")
    else()
        message(FATAL_ERROR "A = '${A}' --> should be '/tree/yz'")
    endif()
endmacro()

simple_test("/tree/x/tree/yz" "/tree/x")
simple_test("/tree/x/tree/yz""/tree/x/")
simple_test("/tree/yz" "/tree/x")
simple_test("/tree/yz" "/tree/x") # Duplicate test
simple_test("/tree/yz" "/tree/yzx")
simple_test("/tree/yz" "/tree/y")
