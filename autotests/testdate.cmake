# Performs some (very limited) tests on the CMake modules shipped by ARPA2CM

#
# Redistribution and use is allowed without limitation.
#    SPDX-License-Identifier: CC0-1.0
#    SPDX-FileCopyrightText: no
#
cmake_minimum_required(VERSION 3.3 FATAL_ERROR)
project(testdate VERSION 0.13 LANGUAGES NONE)

include(modules/MacroGitVersionInfo)

if(NOT 0 EQUAL PROJECT_VERSION_MAJOR)
    message(FATAL_ERROR "Version number (major)")
endif()
if(NOT 13 EQUAL PROJECT_VERSION_MINOR)
    message(FATAL_ERROR "Version number (minor)")
endif()
if(PROJECT_VERSION_PATCH)
    message(FATA_ERROR "Version number (patch is set)")
endif()

message(STATUS "Pre-git: ${PROJECT_VERSION}")

get_project_git_version()

# This won't change anything, since there's no top-level .git here
