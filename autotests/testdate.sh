#! /bin/sh
#
# Performs some (very limited) tests on the CMake modules shipped by ARPA2CM

#
# Redistribution and use is allowed without limitation.
#    SPDX-License-Identifier: CC0-1.0
#    SPDX-FileCopyrightText: no
#

SRC="$1"

test -d "$SRC" || { echo "! No top-level."; exit 1 ; }
test -f "$SRC/CMakeLists.txt" || { echo "! No top-level CMakeLists.txt"; exit 1; }

rm -rf build-testdate
mkdir build-testdate
cp "$SRC/autotests/testdate.cmake" build-testdate/CMakeLists.txt || { echo "! Can't create testing-CMakeLists.txt" ; exit 1 ; }

( cd build-testdate && cmake . -DCMAKE_MODULE_PATH="$SRC" ) || { echo "! CMake failed." ; exit 1; }


