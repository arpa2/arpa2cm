#! /usr/bin/env bash
#
# Builds the repository ARPA2CM

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

. $( dirname $0 )/cilib.sh

cmd cmake_build_self
cmd cmake_package_self
