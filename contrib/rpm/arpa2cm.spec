Name:    arpa2cm
Summary: Additional ARPA2 modules for CMake
Version: 1.0.3
Release: 1%{?dist}

License: BSD-2-Clause
URL:     https://gitlab.com/arpa2/arpa2cm

Source0:        https://gitlab.com/arpa2/%{name}/-/archive/v%{version}/%{name}-v${version}.tar.bz2
BuildArch:      noarch

BuildRequires: make
BuildRequires: cmake

%description
Additional modules for CMake build system needed by ARPA2 components.


%prep
%autosetup -p1 %{?clang:-a1}


%build

%cmake_build

%install
%cmake_install


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif


%files
%license LICENSES/*.txt


%changelog
* Thu Apr 21 2022 Adriaan de Groot <groot@kde.org> - 1.0.3
- Initial rpm attempt
