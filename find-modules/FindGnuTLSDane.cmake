# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2017 Adriaan de Groot <groot@kde.org>
#

# - Try to find GnuTLS DANE extensions
#
# Once done this will define the following CMake variables:
#
#  - GnuTLSDane_FOUND
#  - GnuTLSDane_INCLUDE_DIRS
#  - GnuTLSDane_LIBRARIES
#  - GnuTLSDane_DEFINITIONS
#
# Defines one IMPORTED target if GnuTLS DANE extensions are found:
#  GnuTLS::DANE (library)
#
include(FeatureSummary)

set_package_properties(
    GnuTLSDane
    PROPERTIES
    DESCRIPTION "GnuTLS DANE extensions"
    URL "https://www.gnutls.org/"
)

if(GnuTLSDane_FIND_REQUIRED)
    find_package(GnuTLS REQUIRED)
else()
    find_package(GnuTLS)
endif()
# You can't have GnuTLSDANE without GnuTLS itself
if(NOT GnuTLS_FOUND)
    set(GnuTLSDane_FOUND FALSE)
    return()
endif()

find_package(PkgConfig)
pkg_check_modules(PC_gtlsdane QUIET gnutls-dane)

set(GnuTLSDane_DEFINITIONS ${PC_gtlsdane_CFLAGS_OTHER})

find_path(
    GnuTLSDane_INCLUDE_DIR
    gnutls/dane.h
    HINTS ${PC_gtlsdane_INCLUDEDIR} ${PC_gtlsdane_INCLUDE_DIRS}
)

find_library(
    GnuTLSDane_LIBRARY
    NAMES gnutls-dane
    HINTS ${PC_gtlsdane_LIBDIR} ${PC_gtlsdane_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    GnuTLSDane
    DEFAULT_MSG
    GnuTLSDane_LIBRARY
    GnuTLSDane_INCLUDE_DIR
)
mark_as_advanced(GnuTLSDane_INCLUDE_DIR GnuTLSDane_LIBRARY)

if(GnuTLSDane_FOUND)
    set(GnuTLSDane_INCLUDE_DIRS ${GnuTLSDane_INCLUDE_DIR})
    set(GnuTLSDane_LIBRARIES ${GnuTLSDane_LIBRARY})
    add_library(GnuTLS::DANE UNKNOWN IMPORTED)
    set_target_properties(
        GnuTLS::DANE
        PROPERTIES
            IMPORTED_LOCATION ${GnuTLSDane_LIBRARY}
            INTERFACE_INCLUDE_DIRECTORIES ${GnuTLSDane_INCLUDE_DIR}
    )
endif()
