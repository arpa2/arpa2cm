# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
#

# Try to find libev. This can find either the static, or the shared library.
# Set `LIBEV_USE_STATIC` ON before searching for the library, to pick the
# static one.
#
# Once done this will define
#  LIBEV_FOUND        - System has libev
#  LIBEV_INCLUDE_DIRS - The libev include directories
#  LIBEV_LIBRARIES    - The libraries needed to use libev
#
# Also supports modern naming conventions and sets
#  LibEv_FOUND
# Defines one IMPORTED target
#  LibEv::ev

if(LIBEV_USE_STATIC)
    message(STATUS "LIBEV_USE_STATIC: ON")
else()
    message(STATUS "LIBEV_USE_STATIC: OFF")
endif(LIBEV_USE_STATIC)

find_path(LIBEV_INCLUDE_DIR NAMES ev.h)

if(LIBEV_USE_STATIC)
    find_library(LIBEV_LIBRARY NAMES libev.a)
else()
    find_library(LIBEV_LIBRARY NAMES ev)
endif(LIBEV_USE_STATIC)

if(LIBEV_INCLUDE_DIR)
    file(
        STRINGS "${LIBEV_INCLUDE_DIR}/ev.h" LIBEV_VERSION_MAJOR
        REGEX "^#define[ \t]+EV_VERSION_MAJOR[ \t]+[0-9]+"
    )
    file(
        STRINGS "${LIBEV_INCLUDE_DIR}/ev.h" LIBEV_VERSION_MINOR
        REGEX "^#define[ \t]+EV_VERSION_MINOR[ \t]+[0-9]+"
    )
    string(
        REGEX REPLACE
        "[^0-9]+"
        ""
        LIBEV_VERSION_MAJOR
        "${LIBEV_VERSION_MAJOR}"
    )
    string(
        REGEX REPLACE
        "[^0-9]+"
        ""
        LIBEV_VERSION_MINOR
        "${LIBEV_VERSION_MINOR}"
    )
    set(LIBEV_VERSION "${LIBEV_VERSION_MAJOR}.${LIBEV_VERSION_MINOR}")
    unset(LIBEV_VERSION_MINOR)
    unset(LIBEV_VERSION_MAJOR)
endif()

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBEV_FOUND to TRUE
# if all listed variables are TRUE and the requested version matches.
find_package_handle_standard_args(
    LibEv
    REQUIRED_VARS LIBEV_LIBRARY LIBEV_INCLUDE_DIR
    VERSION_VAR LIBEV_VERSION
)

if(LIBEV_FOUND)
    set(LIBEV_LIBRARIES ${LIBEV_LIBRARY})
    set(LIBEV_INCLUDE_DIRS ${LIBEV_INCLUDE_DIR})
    add_library(LibEv::ev UNKNOWN IMPORTED)
    set_target_properties(
        LibEv::ev
        PROPERTIES
            IMPORTED_LOCATION ${LIBEV_LIBRARY}
            INTERFACE_INCLUDE_DIRECTORIES ${LIBEV_INCLUDE_DIR}
    )
endif(LIBEV_FOUND)

mark_as_advanced(LIBEV_INCLUDE_DIR LIBEV_LIBRARY)

include(FeatureSummary)
set_package_properties(
    LibEv
    PROPERTIES
    URL "http://software.schmorp.de/pkg/libev.html"
    DESCRIPTION "Libev is a high-performance event loop/event model"
)
