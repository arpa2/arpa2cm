# Try to find Log4cpp libraries.

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright (c) 2014, 2015 InternetWide.org and the ARPA2.net project
#    SPDX-FileCopyrightText: Copyright 2017, 2020, Adriaan de Groot <groot@kde.org>
#

# Finds the log4cpp library and its include files for imported target:
#   Log4cpp::Log4cpp Log4cpp library
# Defines the following variables:
#   - Log4cpp_FOUND
#
# Defines the following legacy variables as well:
#   - LOG4CPP_FOUND
#   - LOG4CPP_LIBRARIES
#   - LOG4CPP_INCLUDE_DIRS
#
# If there is no installed package, you can use LOG4CPP_PREFIX to tell
# this module where Log4cpp is installed; it will look in
# lib/ and include/ subdirectories of that prefix.
#

if(LOG4CPP_FOUND)
    return()
endif()

include(FindPackageHandleStandardArgs)

find_library(
    _L4CPP
    log4cpp
    PATHS ${LOG4CPP_PREFIX}/lib64 ${LOG4CPP_PREFIX}/lib32 ${LOG4CPP_PREFIX}/lib
)
find_path(
    LOG4CPP_INCLUDE_DIRS
    log4cpp/Category.hh
    PATHS ${LOG4CPP_PREFIX}/include
)

if(_L4CPP)
    set(LOG4CPP_LIBRARIES ${_L4CPP})
endif()

find_package_handle_standard_args(
    Log4cpp
    REQUIRED_VARS LOG4CPP_LIBRARIES LOG4CPP_INCLUDE_DIRS
)

set(LOG4CPP_FOUND ${Log4cpp_FOUND})

if(LOG4CPP_FOUND)
    add_library(Log4cpp::Log4cpp UNKNOWN IMPORTED)
    set_target_properties(
        Log4cpp::Log4cpp
        PROPERTIES
            IMPORTED_LOCATION "${LOG4CPP_LIBRARIES}"
            INTERFACE_INCLUDE_DIRECTORIES "${LOG4CPP_INCLUDE_DIRS}"
    )
    try_compile(
        _L4CPP_NEEDS_NO_PTHREAD
        ${CMAKE_CURRENT_BINARY_DIR}
        ${CMAKE_CURRENT_LIST_DIR}/check_log4cpp.cpp
        LINK_LIBRARIES ${LOG4CPP_LIBRARIES}
    )
    if(NOT _L4CPP_NEEDS_NO_PTHREAD)
        message(STATUS "Log4cpp doesn't link, try with -pthread")
        set(LOG4CPP_LIBRARIES ${LOG4CPP_LIBRARIES} -pthread)
        # Now the variable is misnamed
        try_compile(
            _L4CPP_NEEDS_NO_PTHREAD
            ${CMAKE_CURRENT_BINARY_DIR}
            ${CMAKE_CURRENT_LIST_DIR}/check_log4cpp.cpp
            LINK_LIBRARIES ${LOG4CPP_LIBRARIES}
            OUTPUT_VARIABLE _L4CPP_LINK
        )
        if(NOT _L4CPP_NEEDS_NO_PTHREAD)
            message(WARNING " .. Cannot find linker arguments for Log4cpp")
            message(WARNING ${_L4CPP_LINK})
        else()
            message(STATUS " .. Log4cpp needs -pthread")
            set_property(
                TARGET Log4cpp::Log4cpp
                APPEND_STRING
                PROPERTY INTERFACE_LINK_OPTIONS "-pthread"
            )
        endif()
    endif()
endif()
