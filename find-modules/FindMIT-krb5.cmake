# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2019 Rick van Rein <rick@openfortress.nl>
#

# Finds the MIT-krb5 libraries as instances of MIT-krb5_xxx
#
#  MIT-krb5_FOUND          - Set to "mitkrb5" if MIT krb5 is found
#  MIT-krb5_VERSION        - Version of the library found
#  MIT-krb5_INCLUDE_DIRS   - Directories to include to get MIT krb5 headers
#  MIT-krb5_LIBRARIES      - Libraries to link against for MIT krb5
#  MIT-krb5_DEFINITIONS    - Definitions to add to the compiler
#
# If MIT-krb5 is found, the following IMPORTED target is also defined:
#  kerberos::mit (library)
# If no **other** Kerberos implementation was found previously, defines
#  kerberos::kerberos (alias)
#
# The configuration can be specialised with a configuration module,
#
#  MIT-krb5_CONFIG_MODULE  - Invoke krb5-config for this module
#
# Since other Kerberos systems exist, notably Heimdal and possibly
# one day Shishi, is some continued work to put the values into
# more general variables:
#
#  Kerberos5_SYSTEM        - Would be set to "mitkrb5" for us
#  Kerberos5_FOUND         - Copied from MIT-krb5_FOUND for "mitkrb5"
#  Kerberos5_VERSION       - Copied from MIT-krb5_VERSION
#  Kerberos5_INCLUDE_DIRS  - Copied from MIT-krb5_INCLUDE_DIRS
#  Kerberos5_LIBRARIES     - Copied from MIT-krb5_LIBRARIES
#  Kerberos5_DEFINITIONS   - Copied from MIT-krb5_DEFINITIONS
#
# The logic of Kerberos5_SYSTEM is "pluggable", in that it will be
# set (with cache) to "mitkrb5" if it is not yet set.  If it has a
# value but not "mitkrb5", none of the Kerberos5_xxx is changed.
#
# Note that **only** MIT-krb5 has the com_err implementation that
# is required to use the Findcom_err.cmake module (Heimdal, which
# is installed by default on FreeBSD systems, has a different API).
#
include(FeatureSummary)

find_package(PkgConfig)

set_package_properties(
    MIT-krb5
    PROPERTIES
    DESCRIPTION "Find MIT-krb5 development goodies"
    URL "http://web.mit.edu/kerberos/krb5-latest/doc/appdev/index.html"
)

set(MIT-krb5_SYSTEM "mitkrb5")

if("${MIT-krb5_FOUND}" STREQUAL "${MIT-krb5_SYSTEM}")
    set(MIT-krb5_QUIETLY TRUE)
endif()

find_program(
    MIT-krb5_CONFIG
    NAMES "krb5-config.mit" "krb5-config" "krb5-config.bat"
) #DOC "Configuration program for MIT-krb5 (validity depends on MIT-krb5_FOUND)")

if(MIT-krb5_CONFIG)
    execute_process(
        COMMAND ${MIT-krb5_CONFIG} --vendor
        OUTPUT_VARIABLE MIT-krb5_VENDOR
        RESULT_VARIABLE _vendor_bad
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if(_vendor_bad)
        set(MIT-krb5_CONFIG NOTFOUND)
    endif()
endif()

# If the krb5-config doesnt recognize --vendor, it's the wrong one,
# we may have reset CONFIG, above .. then we'll use pkgconfig instead.
if(MIT-krb5_CONFIG)
    if("${MIT-krb5_VENDOR}" STREQUAL "Massachusetts Institute of Technology")
        set(MIT-krb5_FOUND TRUE)
    endif()

    execute_process(
        COMMAND ${MIT-krb5_CONFIG} --version
        OUTPUT_VARIABLE MIT-krb5_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    execute_process(
        COMMAND ${MIT-krb5_CONFIG} ${MIT-krb5_CONFIG_MODULE} --cflags
        OUTPUT_VARIABLE MIT-krb5_DEFINITIONS
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    execute_process(
        COMMAND ${MIT-krb5_CONFIG} ${MIT-krb5_CONFIG_MODULE} --libs --deps
        OUTPUT_VARIABLE MIT-krb5_LIBRARIES
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
else()
    pkg_check_modules(MIT-krb5 QUIET mit-krb5)
    if(MIT-krb5_FOUND)
        set(MIT-krb5_DEFINITIONS ${MIT-krb5_CFLAGS})
    endif()
endif()

if(Kerberos5_SYSTEM)
    if(NOT "${Kerberos5_SYSTEM}" STREQUAL "${MIT-krb5_SYSTEM}")
        unset(MIT-krb5_FOUND)
    endif()
endif()

if(MIT-krb5_FOUND)
    if(NOT MIT-krb5_QUIETLY)
        message(STATUS "Found package MIT-krb5 ${MIT-krb5_VERSION}")
        message(STATUS "  DEFINITIONS \"${MIT-krb5_DEFINITIONS}\"")
        message(STATUS "  LIBRARIES   \"${MIT-krb5_LIBRARIES}\"")
    endif()
    # NOTE: we don't have an IMPORTED_LOCATION because the settings
    #       obtained from krb5-config don't necessarily give us
    #       **one** library to link to.
    if(NOT TARGET kerberos::mit)
        add_library(kerberos::mit UNKNOWN IMPORTED)
        set_target_properties(
            kerberos::mit
            PROPERTIES
                INTERFACE_LINK_LIBRARIES $MIT-krb5_LIBRARIES}
                INTERFACE_COMPILE_DEFINITIONS ${MIT-krb5_DEFINITIONS}
        )
    endif()
    if(NOT TARGET kerberos::kerberos)
        add_library(kerberos::kerberos ALIAS kerberos::mit)
    endif()
else()
    if(MIT-krb5_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find REQUIRED package MIT-krb5")
    else()
        message(STATUS "Optional package MIT-krb5 was not found")
    endif()
endif()

# CMakeFile Constraint: set_property (CACHE Kerberos5_SYSTEM PROPERTY STRINGS mitkrb5 heimdal ad shishi)
if(NOT Kerberos5_SYSTEM)
    set(Kerberos5_SYSTEM ${MIT-krb5_SYSTEM}
        CACHE STRING "The selected Kerberos implementation"
    )
endif()

if("${Kerberos5_SYSTEM}" STREQUAL "${MIT-krb5_SYSTEM}")
    set(Kerberos5_FOUND "${MIT-krb5_FOUND}")
    set(Kerberos5_VERSION "${MIT-krb5_VERSION}")
    set(Kerberos5_INCLUDE_DIRS "${MIT-krb5_INCLUDE_DIRS}")
    set(Kerberos5_LIBRARIES "${MIT-krb5_LIBRARIES}")
    set(Kerberos5_DEFINITIONS "${MIT-krb5_DEFINITIONS}")
    if(NOT MIT-krb5-QUIETLY)
        message(STATUS "Defined Kerberos5_SYSTEM as \"${MIT-krb5_SYSTEM}\"")
    endif()
endif()
