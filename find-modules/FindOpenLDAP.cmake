# Try to find OpenLDAP client libraries. Sets standard variables
# OpenLDAP_LIBRARIES and OpenLDAP_INCLUDE_DIRS to get both
# LDAP and BER; Sets OpenLDAP_LIBRARY and OpenLDAP_BER_LIBRARY
# for the individual libs.
#
# Provides imported libraries `OpenLDAP::ldap` and `OpenLDAP::ber`.

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright (c) 2014, 2015 InternetWide.org and the ARPA2.net project
#    SPDX-FileCopyrightText: Copyright 2017, 2020, Adriaan de Groot <groot@kde.org>
#

include(FindPackageHandleStandardArgs)

find_library(OpenLDAP_LIBRARY ldap)
find_library(OpenLDAP_BER_LIBRARY lber)

find_path(OpenLDAP_INCLUDE_DIR ldap.h)

find_package_handle_standard_args(
    OpenLDAP
    REQUIRED_VARS OpenLDAP_LIBRARY OpenLDAP_INCLUDE_DIR
)
mark_as_advanced(OpenLDAP_LIBRARY OpenLDAP_BER_LIBRARY OpenLDAP_INCLUDE_DIR)

if(OpenLDAP_FOUND)
    set(OpenLDAP_LIBRARIES ${OpenLDAP_LIBRARY})
    if(OpenLDAP_LIBRARY AND OpenLDAP_BER_LIBRARY)
        list(APPEND OpenLDAP_LIBRARIES ${OpenLDAP_BER_LIBRARY})
    endif()

    set(OpenLDAP_INCLUDE_DIRS ${OpenLDAP_INCLUDE_DIR})

    add_library(OpenLDAP::ldap UNKNOWN IMPORTED)
    set_property(
        TARGET OpenLDAP::ldap
        PROPERTY IMPORTED_LOCATION "${OpenLDAP_LIBRARY}"
    )
    set_property(
        TARGET OpenLDAP::ldap
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${OpenLDAP_INCLUDE_DIR}"
    )
    if(OpenLDAP_BER_LIBRARY)
        add_library(OpenLDAP::ber UNKNOWN IMPORTED)
        set_property(
            TARGET OpenLDAP::ber
            PROPERTY IMPORTED_LOCATION "${OpenLDAP_BER_LIBRARY}"
        )
        set_property(
            TARGET OpenLDAP::ber
            PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${OpenLDAP_INCLUDE_DIR}"
        )
    endif()
endif()
