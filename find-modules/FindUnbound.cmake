# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2017 Adriaan de Groot <groot@kde.org>
#

# Copied from the Fenrir project, and drastically modified to
# match modern CMake style.
# Once done this will define the following CMake variables:
#
#  - Unbound_FOUND
#  - Unbound_INCLUDE_DIRS
#  - Unbound_LIBRARIES
#
# Defines one IMPORTED target:
#  DNS::unbound (library; see also FindLibldns)
#

find_path(
    Unbound_INCLUDE_DIR
    NAMES unbound.h
    PATH_SUFFIXES include/ include/unbound/
    PATHS
        "${PROJECT_SOURCE_DIR}"
        ${UNBOUND_ROOT}
        $ENV{UNBOUND_ROOT}
        /usr/local/
        /usr/
)

if(Unbound_INCLUDE_DIR)
    set(Unbound_FOUND TRUE)
else()
    set(Unbound_FOUND FALSE)
endif()

if(Unbound_FOUND)
    message(STATUS "Found unbound in ${Unbound_INCLUDE_DIR}")
else()
    if(Unbound_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find \"unbound\" library")
    endif()
endif()

find_library(Unbound_LIBRARY NAMES unbound HINTS /usr/local /usr)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    Unbound
    DEFAULT_MSG
    Unbound_LIBRARY
    Unbound_INCLUDE_DIR
)
mark_as_advanced(Unbound_INCLUDE_DIR Unbound_LIBRARY)

if(Unbound_FOUND)
    set(Unbound_INCLUDE_DIRS ${Unbound_INCLUDE_DIR})
    set(Unbound_LIBRARIES ${Unbound_LIBRARY})
    add_library(DNS::unbound UNKNOWN IMPORTED)
    set_target_properties(
        DNS::unbound
        PROPERTIES
            IMPORTED_LOCATION ${Unbound_LIBRARY}
            INTERFACE_INCLUDE_DIRECTORIES ${Unbound_INCLUDE_DIR}
    )
endif()

include(FeatureSummary)
set_package_properties(
    Unbound
    PROPERTIES
    URL "https://nlnetlabs.nl/projects/unbound/"
    DESCRIPTION "Unbound is a validating, recursive, caching DNS resolver"
)
