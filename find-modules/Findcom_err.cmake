# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>
#

# Finds the com_err support libraries and compiler
#
#  com_err_FOUND           - Set if compile_et is found
#  com_err_COMPILER        - Set to the compile_et compiler path
#  com_err_VERSION         - Set to the version of com_err
#  com_err_CFLAGS          - Set to the C compiler flags for com_err
#  com_err_LIBRARIES       - Set to the libraries for com_err
#
# There is one imported target (which is rarely used)
#
#  com_err::com_err        - the global com_err library
#
# This module **prefers** the com_err support libraries found via
# pkg-config; this is important only when mixing FreeBSD base com_et
# with MIT-krb5 com_et, which have subtly-different APIs.
#
#
# This module also defines convenience commands to use the com_err compiler:
#
#  add_com_err_table(tablename [SOURCE src] [EXPORT name])
#
# which will build an error table called *tablename*. By default, the
# source file is called `tablename.et`, but the optional SOURCE
# parameter can change the source file name. Do **not** include the
# `.et` file suffix in the *src* name. This **also** defines a library
# target *com_err::tablename* which can be linked to (so you don't need
# to call use_com_err_table() in that case).
#
# If the optional EXPORT argument is given, the table is added to the
# given export group.
#
#
#  use_com_err_table(tablename [LINK] TARGETS tgt..)
#
# This adds suitable include directories to all the targets so that
# they can find the error headers from a previous add_com_err_table()
# invocation. If the LINK keyword is used, also link to the table.
#
#  use_com_err_table(tablename ALL_TARGETS)
#
# This just does an include_directories() for the directory under
# which the error table header is generated.
#
#
# The common usage pattern for consumers of error numbers **only**:
#
#  add_com_err_table     (my-complaints)  # Only once, somewhere
#  add_executable        (my-sillyness silly.c)
#  use_com_err_table     (my-complaints TARGETS my-sillyness)
#
# The common usage pattern for consumers of the error message table:
#
#  add_com_err_table     (my-complaints)  # Only once, somewhere
#  add_executable        (my-sillyness silly.c)
#  target_link_libraries (my-sillyness PUBLIC com_err::my-complaints)
#
#
# The add_com_err_table() function adds a .et file from local source code.
# It is only needed in the directory holding the .et file.
#
# The use_com_err_table() function allows the targets that it is
# given to find the locally-built error table headers (under com_err/)
# and links in the table itself.
#
include(FeatureSummary)

set_package_properties(
    com_err
    PROPERTIES
    DESCRIPTION "Find com_err development tools"
    URL "See MIT krb5 package"
)

find_package(PkgConfig)
find_program(com_err_COMPILER NAMES "compile_et") #DOC "Compiler for application-specific error code/message tables")

if(com_err_COMPILER AND PkgConfig_FOUND)
    # Look here for alternative compile_et versions
    set(com_err_PREFIX "")
    if(CMAKE_SYSTEM_NAME MATCHES "FreeBSD")
        # On FreeBSD, we do not want the base-system one, we need
        # compile_et from MIT-krb5 (for consistency *later* in the
        # stack, when we require MIT-krb5; mixing com_err versions
        # is a bad idea).
        pkg_check_modules(MIT-krb5 QUIET mit-krb5)
        if(MIT-krb5_FOUND)
            find_library(_ce_library com_err PATHS ${MIT-krb5_LIBRARY_DIRS})
            if(_ce_library)
                set(com_err_LIBRARIES "${_ce_library}")
                set(com_err_FOUND TRUE)
            endif()
            # Get the compile_et from MIT
            pkg_get_variable(com_err_PREFIX mit-krb5 exec_prefix)
        else()
            message(WARNING "No MIT-krb5 found, com_err is likely broken.")
            set(com_err_LIBRARIES "com_err")
            # This is disputable, it's probably base-system com_err and
            # will break *somewhere* down the line.
            set(com_err_FOUND TRUE)
        endif()
    else()
        # This just fills in the com_err_* variables; we don't
        # especially need them, since we have the compiler.
        # So if pkg-config doesn't find it, **still** pretend it's there
        # (for instance, on FreeBSD it is part of the base system).
        pkg_search_module(com_err com_err)

        # Prefer an
        set(com_err_PREFIX "")
        if(com_err_FOUND)
            pkg_get_variable(com_err_PREFIX com_err exec_prefix)
        endif()
        set(com_err_FOUND TRUE)
        set(com_err_LIBRARIES "com_err")
    endif()
    # If there's a prefix, check for a prefix-specific compile_et
    if(com_err_PREFIX)
        find_program(
            com_err_COMPILER_alt
            NAMES "compile_et"
            PATHS ${com_err_PREFIX} ${com_err_PREFIX}/bin
            NO_DEFAULT_PATH
        )
        if(com_err_COMPILER_alt)
            set(com_err_COMPILER ${com_err_COMPILER_alt})
        endif()
    endif()
else()
    if(NOT PkgConfig_FOUND)
        set(_msg
            "No PkgConfig was found.\nPkgConfig is REQUIRED to be able to look for com_err."
        )
        if(CMAKE_SYSTEM_NAME MATCHES "FreeBSD")
            string(
                APPEND
                _msg
                "\nTo find com_err, you need package 'krb5' (MIT Kerberos) and 'pkgconf'"
            )
        endif()
        message(SEND_ERROR "${_msg}")
    endif()
    set(com_err_FOUND FALSE)
endif()

if(com_err_FOUND)
    if(NOT com_err_QUIETLY)
        message(STATUS "Found package com_err ${com_err_VERSION}")
        message(STATUS "  DEFINITIONS \"${com_err_CFLAGS}\"")
        message(STATUS "  LIBRARIES   \"${com_err_LIBRARIES}\"")
        message(STATUS "  COMPILER    \"${com_err_COMPILER}\"")
    endif()
    add_library(com_err::com_err UNKNOWN IMPORTED)
    set_property(
        TARGET com_err::com_err
        PROPERTY IMPORTED_LOCATION "${com_err_LIBRARIES}"
    )
else()
    if(com_err_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find REQUIRED package com_err")
    else()
        message(STATUS "Optional package com_err was not found")
    endif()
endif()

function(use_com_err_table _tablename)
    set(TABLENAME ${_tablename})
    if(TARGET com_err::${_tablename})
        message(STATUS "Using table com_err::${_tablename}")
    else()
        # Assuming an ARPA2 com_err table, it is exported under a different name
        if(TARGET ARPA2::com_err_X_${_tablename})
            message(STATUS "Imported ARPA2 table com_err::${_tablename}")
            add_library(
                com_err::${_tablename}
                ALIAS
                ARPA2::com_err_X_${_tablename}
            )
        endif()
        #
        # If there still isn't a com_err::${_tablename} target later
        # commands will fail because of a missing target. There isn't
        # much we can do about that.
    endif()
    get_target_property(
        include_dir
        com_err::${_tablename}
        INTERFACE_INCLUDE_DIRECTORIES
    )

    cmake_parse_arguments(_acet "ALL_TARGETS;LINK" "" "TARGETS" ${ARGN})
    if(_acet_ALL_TARGETS)
        if(_acet_LINK)
            message(
                FATAL_ERROR
                "use_com_err_table cannot mix LINK with ALL_TARGETS"
            )
        endif()
        include_directories(${include_dir})
        return()
    else()
        if(NOT _acet_TARGETS)
            message(FATAL_ERROR "use_com_err_table needs TARGETS")
        endif()
    endif()

    foreach(t ${_acet_TARGETS})
        # If this table is being made in the same project, then we need it
        # to have-been-built before trying to use it. Otherwise we might
        # end up #include'ing the headers before they have been generated.
        if(TARGET com_err_${_tablename}_DEPENDENCIES)
            add_dependencies(${t} com_err_${_tablename}_DEPENDENCIES)
        endif()
        if(_acet_LINK)
            target_link_libraries(${t} INTERFACE com_err::${_tablename})
        else()
            target_include_directories(${t} PRIVATE ${include_dir})
        endif()
    endforeach()
endfunction()

function(add_com_err_table _tablename)
    set(TABLENAME ${_tablename})
    set(singlevalued SOURCE EXPORT)
    cmake_parse_arguments(_acet "" "${singlevalued}" "" ${ARGN})
    if(_acet_SOURCE)
        set(TABLESRC ${_acet_SOURCE})
    else()
        set(TABLESRC ${TABLENAME})
    endif()

    set(_outputdir ${CMAKE_BINARY_DIR}/include/com_err)
    set(_outputflag ${CMAKE_CURRENT_BINARY_DIR}/include.com_err.dir)

    # Any add_com_err_table invocation can create the output directory
    add_custom_command(
        OUTPUT ${_outputflag}
        COMMAND cmake -E make_directory ${_outputdir}
        COMMAND cmake -E touch ${_outputflag}
    )
    add_custom_command(
        OUTPUT ${_outputdir}/${TABLENAME}.et
        COMMAND
            cmake -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${TABLESRC}.et
            ${_outputdir}/${TABLENAME}.et
        DEPENDS ${_outputflag}
    )
    add_custom_command(
        OUTPUT ${_outputdir}/${TABLENAME}.h ${_outputdir}/${TABLENAME}.c
        COMMAND ${com_err_COMPILER} ${TABLENAME}.et
        DEPENDS ${_outputdir}/${TABLENAME}.et
        WORKING_DIRECTORY ${_outputdir}
    )
    add_custom_target(
        com_err_${TABLENAME}_DEPENDENCIES
        ALL
        DEPENDS
            ${_outputdir}/${TABLENAME}.h
            ${_outputdir}/${TABLENAME}.c
            ${_outputdir}/${TABLENAME}.et
            ${CMAKE_CURRENT_SOURCE_DIR}/${TABLESRC}.et
    )
    install(FILES ${_outputdir}/${TABLENAME}.h DESTINATION include/com_err)

    add_library(com_err_X_${TABLENAME} STATIC ${_outputdir}/${TABLENAME}.c)
    target_compile_options(
        com_err_X_${TABLENAME}
        PRIVATE ${com_err_CFLAGS} ${CMAKE_C_COMPILE_OPTIONS_PIC}
    )

    # The generated code is flagged with warnings when compiled with
    # Clang (e.g. because it does not include the com_err headers, and
    # so contains an implicit declaration of add_error_table() ). Massage
    # the compile flags for those targets to avoid the warnings.
    if(CMAKE_C_COMPILER_ID MATCHES "Clang")
        target_compile_options(
            com_err_X_${TABLENAME}
            PRIVATE -Wno-implicit-function-declaration
        )
    endif()
    target_include_directories(
        com_err_X_${TABLENAME}
        PUBLIC
            $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/include>
            $<INSTALL_INTERFACE:include/com_err>
    )
    target_link_libraries(com_err_X_${TABLENAME} PUBLIC ${com_err_LIBRARIES})
    add_library(com_err::${TABLENAME} ALIAS com_err_X_${TABLENAME})
    if(_acet_EXPORT)
        install(
            TARGETS com_err_X_${TABLENAME}
            EXPORT ${_acet_EXPORT}
            ARCHIVE
            DESTINATION lib
        )
    endif()
endfunction()
