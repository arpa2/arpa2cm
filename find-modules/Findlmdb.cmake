# Part of ARPA2CM (https://gitlab.com/arpa2/arpa2cm/)
#
#    SPDX-License-Identifier: MIT
#    SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
#

# Finds lmdb (Lightning Memory-Mapped Database)
#
#  lmdb_FOUND          - True if lmdb is found.
#  lmdb::lmdb          - Imported target for lmdb
#
# For legacy use, the following variables are also set:
#  LMDB_FOUND          - True if lmdb is found.
#  LMDB_INCLUDE_DIR    - Directory to include to get lmdb headers
#  LMDB_LIBRARY        - Library to link against for lmdb
#
if(TARGET lmdb::lmdb)
    # We've been here before
    return()
endif()

include(CheckLibraryExists)
include(FeatureSummary)

set_package_properties(
    LMDB
    PROPERTIES
    DESCRIPTION "LMDB support (persistent database)"
    URL "https://github.com/LMDB/lmdb"
    # URL "https://symas.com/lmdb/"
)

if(LMDB_INCLUDE_DIR AND LMDB_LIBRARY)
    # Already in cache, be silent
    set(LMDB_FIND_QUIETLY TRUE)
endif()

### Look for the header file.
#
# Do it once with hints and no default, so that a version
# installed in the hints paths will be preferred over a system version.
#
find_path(
    LMDB_INCLUDE_DIR
    NAMES lmdb.h
    HINTS /usr/local/include
    DOC "Include directory for lmdb"
    NO_DEFAULT_PATH
)
if(NOT LMDB_INCLUDE_DIR)
    find_path(LMDB_INCLUDE_DIR NAMES lmdb.h DOC "Include directory for lmdb")
endif()
mark_as_advanced(LMDB_INCLUDE_DIR)

### Look for the library.
#
# If it exists, check for mdb_txn_begin
find_library(
    LMDB_LIBRARY
    NAMES lmdb
    HINTS /usr/local/lib
    DOC "Libraries to link against for lmdb"
)
if(LMDB_LIBRARY)
    check_library_exists(${LMDB_LIBRARY} mdb_txn_begin "" _have_lmdb)
    if(NOT _have_lmdb)
        if(LMDB_FIND_REQUIRED OR NOT LMDB_FIND_QUIETLY)
            message(
                STATUS
                "Found lmdb in ${LMDB_LIBRARY} but it is missing mdb_txn_begin"
            )
        endif()
    endif()
endif()
mark_as_advanced(LMDB_LIBRARY)

# Copy the results to the output variables.
if(LMDB_INCLUDE_DIR AND LMDB_LIBRARY AND _have_lmdb)
    set(LMDB_FOUND TRUE)
else()
    set(LMDB_FOUND FALSE)
endif()

# .. and in the variables that match the module name.
set(lmdb_FOUND ${LMDB_FOUND})

if(LMDB_FOUND)
    if(NOT LMDB_FIND_QUIETLY)
        message(STATUS "Found lmdb header files: ${LMDB_INCLUDE_DIR}")
        message(STATUS "Found lmdb libraries:    ${LMDB_LIBRARY}")
    endif()
    add_library(lmdb::lmdb UNKNOWN IMPORTED)
    set_property(TARGET lmdb::lmdb PROPERTY IMPORTED_LOCATION "${LMDB_LIBRARY}")
    set_property(
        TARGET lmdb::lmdb
        PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${LMDB_INCLUDE_DIR}"
    )
else()
    if(LMDB_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find lmdb")
    elseif(NOT LMDB_FIND_QUIETLY)
        message(STATUS "Optional package lmdb was not found")
    endif()
endif()
