# ExceptionHandler.cmake -- Portable definitions for exception handling
# This selects the log output mechanism for #incude <arpa2/except.h>
#
# This is input to #include <arpa2/except.h> as documented therein.

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>
#

set(LOG_STYLE "LOG_STYLE_DEFAULT"
    CACHE STRING
    "Log style: NULL for no log, STDOUT / STDERR / SYSLOG for the respective channel, DEFAULT for a platform-sensible default"
)

if(NOT LOG_STYLE)
    set(LOG_STYLE LOG_STYLE_DEFAULT)
endif()

set_property(
    CACHE LOG_STYLE
    PROPERTY
        STRINGS
        LOG_STYLE_NULL
        LOG_STYLE_DEFAULT
        LOG_STYLE_STDOUT
        LOG_STYLE_STDERR
        LOG_STYLE_SYSLOG
)

add_compile_definitions(LOG_STYLE=${LOG_STYLE})
