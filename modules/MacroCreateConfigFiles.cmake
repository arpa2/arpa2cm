# CREATE_CONFIG_FILES(<packagename> [options])
#    Call this macro to generate CMake and pkg-config configuration
#    files from templates found in the top-level source directory.
#
#       (option) NO_PKGCONFIG
#           Set this to skip the pkg-config (.pc) file. No file will
#           be installed for pkg-config.
#       (option) EXPORT
#           Install an additional Targets file for the targets exported
#           (in CMake) for package <packagename>. This is recommended.
#
# Most ARPA2-related components write configuration-information
# files and install them. There are two flavors:
#
#   - CMake config-info files (<foo>Config.cmake and <foo>ConfigVersion.cmake)
#   - pkg-config files (<foo>.pc)
#
# The macro create_config_files() simplifies this process
# by using named template files for all three output files.
# Pass a package name (e.g. "Quick-DER") to the macro, and
# the source files (e.g. <file>.in for the files named above
# will be taken from the top-level contrib/{cmake,pkgconfig}
# source directories.
#
# As an (un)special case, the ConfigVersion file may be taken from
# the cmake/ directory, since there is nothing particularly special
# for that file (as opposed to the other files, which need to
# specify paths, dependencies, and other things).
#
# The variable <packagename>_DIR is set in the caller's scope, as
# if find_package(<packagename>) had been called by a consumer.
# This is the destination-directory to use for other files
# (eg. CMake support modules) for use with the package.
#
# Consumers of older ARPA2CM versions can use the following code instead
# (which duplicates the internals used to set <packagename>_DIR here).
# Be sure to substitute in the value of <packagename>.
#
# ```
# if(WIN32 AND NOT CYGWIN)
#     set (INSTALL_CMAKE_DIR CMake)
# else()
#     set (INSTALL_CMAKE_DIR ${SHARE_INSTALL_DIR}/<packagename>/cmake/)
# endif()
# ```
#

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
#

include(CMakePackageConfigHelpers)

set(SHARE_INSTALL_DIR share CACHE PATH "read-only architecture-independent data")

function(create_config_files _packagename)
    export(PACKAGE ${_packagename})

    cmake_parse_arguments(_acm_ccf "NO_PKGCONFIG;EXPORT" "" "" ${ARGN})

    # The CMake configuration files are written to different locations
    # depending on the host platform, since different conventions apply.
    if(WIN32 AND NOT CYGWIN)
        set(INSTALL_CMAKE_DIR CMake)
    else()
        set(INSTALL_CMAKE_DIR ${SHARE_INSTALL_DIR}/${_packagename}/cmake/)
    endif()
    set(${_packagename}_DIR ${INSTALL_CMAKE_DIR} PARENT_SCOPE)

    # pkg-config bits
    if(NOT _acm_ccf_NO_PKGCONFIG)
        configure_file(
            "${PROJECT_SOURCE_DIR}/contrib/pkgconfig/${_packagename}.pc.in"
            "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}.pc"
            @ONLY
        )
        install(
            FILES
                "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}.pc"
            DESTINATION "lib/pkgconfig/"
            COMPONENT dev
        )
    endif()

    # CMake support bits
    set(LIB_DIR ${CMAKE_INSTALL_PREFIX}/lib)
    set(INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include)
    set(SHARE_DIR ${CMAKE_INSTALL_PREFIX}/share)
    configure_package_config_file(
        "${PROJECT_SOURCE_DIR}/contrib/cmake/${_packagename}Config.cmake.in"
        "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}Config.cmake"
        INSTALL_DESTINATION "${INSTALL_CMAKE_DIR}"
        PATH_VARS INCLUDE_DIR LIB_DIR SHARE_DIR
    )
    write_basic_package_version_file(
        ${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}ConfigVersion.cmake
        VERSION ${${_packagename}_VERSION}
        COMPATIBILITY SameMajorVersion
    )
    install(
        FILES
            "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}Config.cmake"
            "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${_packagename}ConfigVersion.cmake"
        DESTINATION "${INSTALL_CMAKE_DIR}"
        COMPONENT dev
    )
    if(_acm_ccf_EXPORT)
        install(
            EXPORT ${_packagename}
            DESTINATION "${INSTALL_CMAKE_DIR}"
            FILE "${_packagename}Targets.cmake"
            NAMESPACE ARPA2::
        )
    endif()
endfunction()
