# MACRO_ENSURE_OUT_OF_SOURCE_BUILD(<errorMessage>)
#    Call this macro in your project if you want to enforce out-of-source builds.
#    If an in-source build is detected, it will abort with the given error message.
#    This macro works in any of the CMakeLists.txt of your project, but the recommended
#    location to call this is close to the beginning of the top level CMakeLists.txt

#
# Redistribution and use is allowed according to the terms of the 3-clause BSD license.
#    SPDX-License-Identifier: BSD-3-Clause
#    SPDX-FileCopyrightText: Copyright (c) 2006, Alexander Neundorf, <neundorf@kde.org>
#

macro(MACRO_ENSURE_OUT_OF_SOURCE_BUILD _errorMessage)
    if(CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
        message(FATAL_ERROR "${_errorMessage}")
    endif()
endmacro(MACRO_ENSURE_OUT_OF_SOURCE_BUILD)
