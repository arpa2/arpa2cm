# GET_PROJECT_GIT_VERSION()
#    Uses git tags to add a .TWEAK as needed to the VERSION variables.
#    Uses settings from the project() command as a starting point, and
#    follows "new" versioning style (following semver tags: vM.m.p).
#
# GET_VERSION_FROM_GIT(<appname> <default>)
#    Uses git tags to determine a version name and sets <appname>_VERSION
#    (along with split-out _MAJOR, _MINOR and _PATCHLEVEL variables). If
#    git isn't available, use <default> for version information (which should
#    be a string in the format M.m.p). This macro is **deprecated** and
#    causes a warning to be printed.
#
#
# Version Information
#
# This assumes you are working from a git checkout that uses tags
# in an orderly fashion (e.g. according to the ARPA2 project best
# practices guide, with version-* tags). It also checks for local
# modifications and uses that to munge the <patchlevel> part of
# the version number.
#
# Produces version numbers <major>.<minor>.<patchlevel>.
#
# To use the macro, provide an app- or package name; this is
# used to fill variables called <app>_VERSION_MAJOR, .. and
# overall <app>_VERSION. If git can't be found or does not Produce
# meaningful output, use the provided default, e.g.:
#
#   get_version_from_git(Quick-DER 0.1-5)
#
# After the macro invocation, Quick-DER_VERSION is set according
# to the git tag or 0.1.5. Note that the provided default version
# **MUST** have the format x.y-z (with a dash) or x.y (in which
# case -0 is assumed). Format x.y is preferred.
#
# Git tags **MUST** have names like "version-x.y" (including
# the "version-" prefix).

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
#    SPDX-FileCopyrightText: Copyright 2017, Adriaan de Groot <groot@kde.org>
#

set(_arpa2cm_git_version_header ${CMAKE_CURRENT_LIST_DIR}/VersionHeader.h.in)

macro(GET_PROJECT_GIT_VERSION)
    set(_arpa2_PROJECT_VERSION "v${PROJECT_VERSION}")
    if(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/.git)
        find_package(Git QUIET)
        if(Git_FOUND)
            message(STATUS "Looking for git-versioning information.")
            execute_process(
                COMMAND
                    ${GIT_EXECUTABLE} describe --tags --match v[0-9]*.*.*
                    --dirty
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                OUTPUT_VARIABLE GIT_VERSION_INFO
                RESULT_VARIABLE GIT_ERROR
            )
            if(NOT GIT_ERROR)
                set(_arpa2_PROJECT_VERSION "${GIT_VERSION_INFO}")
                string(
                    REGEX REPLACE
                    "^v([1-9][0-9]*|0)[.]([1-9][0-9]*|0)[.]([1-9][0-9]*|0).*$"
                    "\\3"
                    GIT_VERSION_PATCH
                    ${GIT_VERSION_INFO}
                )
                string(
                    REGEX REPLACE
                    "^v[0-9.]*-([0-9]*)-.*$"
                    "\\1"
                    GIT_VERSION_TWEAK
                    ${GIT_VERSION_INFO}
                )
                set(_tweaked OFF)
                if(GIT_VERSION_PATCH AND NOT ${${PROJECT_NAME}_VERSION_PATCH})
                    set(PROJECT_VERSION_PATCH ${GIT_VERSION_PATCH})
                    set(${PROJECT_NAME}_VERSION_PATCH ${PROJECT_VERSION_PATCH})
                    set(_tweaked ON)
                endif()
                if(
                    GIT_VERSION_TWEAK
                    AND NOT "${GIT_VERSION_TWEAK}" STREQUAL "${GIT_VERSION_INFO}"
                )
                    set(PROJECT_VERSION_TWEAK ${GIT_VERSION_TWEAK})
                    set(${PROJECT_NAME}_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
                    set(_tweaked ON)
                endif()
                if(_tweaked)
                    if("" STREQUAL "${PROJECT_VERSION_PATCH}")
                        set(PROJECT_VERSION_PATCH "0")
                    endif()
                    message(
                        STATUS
                        "Git adjusted versioning: ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}.${PROJECT_VERSION_TWEAK}"
                    )
                endif()
            endif()
        endif()
    endif()
    string(MAKE_C_IDENTIFIER "${PROJECT_NAME}" _arpa2_PROJECT_ID)
    configure_file(
        "${_arpa2cm_git_version_header}"
        "${CMAKE_BINARY_DIR}/VersionHeader.h"
        @ONLY
    )
endmacro()
