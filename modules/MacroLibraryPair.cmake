# Build two targets, *name*_shared and *name*_static, from the given SOURCES.
#
# Many ARPA2 components produce pairs of libraries: one shared, one static.
# This convenience macro builds the two libraries efficiently and handles
# differences in linking static and shared libraries.
#
# library_pair(name
#   [OUTPUT_NAME output]      # Change output name, defaults to name
#   SOURCES src...            # Source files for the library pair
#   [LIBRARIES lib...]        # Libraries for both to link to
#   [SHARED_LIBRARIES lib...] # Link only the shared library to these
#   [STATIC_LIBRARIES lib...] # Link only the static library to these
#   [PAIRED_LIBRARIES lib...] # Link pair-generated ARPA2 libraries
#   [INTERFACE_INCLUDE dir]   # If set export that include directory
#   [EXPORT exportname]       # If set, install() to the given exportname
# )
#
# This produces two library targets:
#   - *name*_shared
#   - *name*_static
# which can be used inside the project for further linking.
#
# You can use OUTPUT_NAME to change the OUTPUT_NAME of the libraries,
# otherwise you will get lib*name*.a and lib*name*.so (on UNIX platforms).
#
# The PAIRED_LIBRARIES are special, for each *lib* listed there, this is
# like adding ARPA2::*lib* to SHARED_LIBRARIES and ARPA2::*lib*-Static
# to STATIC_LIBRARIES. This avoids having to list them twice.
#
#
# The following functions add include- or link-directories to the PUBLIC
# interface of the pair of targets.
#
# library_pair_include_directories(name dir...)
# library_pair_link_directories(name dir...)
#

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

function(library_pair _name)
    set(NAME ${_name})
    set(singlevalued OUTPUT_NAME INTERFACE_INCLUDE EXPORT)
    set(multivalued
        SOURCES
        LIBRARIES
        SHARED_LIBRARIES
        STATIC_LIBRARIES
        PAIRED_LIBRARIES
    )
    cmake_parse_arguments(_lp "" "${singlevalued}" "${multivalued}" ${ARGN})

    if(NOT _lp_OUTPUT_NAME)
        set(_lp_OUTPUT_NAME ${NAME})
    endif()

    # Expand the argument PAIRED_LIBRARIES to shared & static names in the
    # SHARED_LIBRARIES and STATIC_LIBRARIES lists.
    foreach(_lp_lib ${_lp_PAIRED_LIBRARIES})
        if(TARGET ${_lp_lib}_shared AND TARGET ${_lp_lib}_static)
            # These are in the current project
            list(APPEND _lp_SHARED_LIBRARIES ${_lp_lib}_shared)
            list(APPEND _lp_STATIC_LIBRARIES ${_lp_lib}_static)
        else()
            list(APPEND _lp_SHARED_LIBRARIES ARPA2::${_lp_lib})
            list(APPEND _lp_STATIC_LIBRARIES ARPA2::${_lp_lib}-Static)
        endif()
    endforeach()

    add_library(${NAME}_shared SHARED ${_lp_SOURCES})
    add_library(${NAME}_static STATIC ${_lp_SOURCES})
    add_library(${NAME} ALIAS ${NAME}_shared)
    target_compile_options(${NAME}_static PRIVATE -fPIC)

    target_link_libraries(
        ${NAME}_shared
        PUBLIC ${_lp_LIBRARIES} ${_lp_SHARED_LIBRARIES}
    )
    target_link_libraries(
        ${NAME}_static
        PUBLIC ${_lp_LIBRARIES} ${_lp_STATIC_LIBRARIES}
    )
    set_target_properties(
        ${NAME}_shared
        PROPERTIES OUTPUT_NAME ${_lp_OUTPUT_NAME} EXPORT_NAME ${NAME}
    )
    set_target_properties(
        ${NAME}_static
        PROPERTIES OUTPUT_NAME ${_lp_OUTPUT_NAME} EXPORT_NAME ${NAME}-Static
    )

    if(_lp_INTERFACE_INCLUDE)
        set_property(
            TARGET ${NAME}_shared ${NAME}_static
            PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${_lp_INTERFACE_INCLUDE}
        )
    endif()
    if(_lp_EXPORT)
        install(
            TARGETS ${NAME}_shared ${NAME}_static
            EXPORT ${_lp_EXPORT}
            LIBRARY
            DESTINATION lib
            ARCHIVE
            DESTINATION lib
            RUNTIME
            DESTINATION bin
        )
    endif()
endfunction()

function(library_pair_include_directories _name)
    target_include_directories(${_name}_shared PUBLIC ${ARGN})
    target_include_directories(${_name}_static PUBLIC ${ARGN})
endfunction()

function(library_pair_link_directories _name)
    target_link_directories(${_name}_shared PUBLIC ${ARGN})
    target_link_directories(${_name}_static PUBLIC ${ARGN})
endfunction()
