# Strip a DESTDIR from a path
#
# When a package is installed into a DESTDIR and subsequently
# used as a dependency from that DESTDIR, it may end referencing
# paths within the DESTDIR in an absolute sense. For instance,
# library paths may look like ${DESTDIR}/usr/lib/libexample.so,
# or a plugin directory may end up specified as a full path
# ${DESTDIR}/usr/lib/example/plugins/ .
#
# This is normal behavior: if the software is installed in DESTDIR,
# that is where it is even if **logically** the DESTDIR shouldn't
# matter, or should be considered an unimportant detail -- the
# path inside the DESTDIR is the important bit (e.g. /usr/lib).
#
# When using mkhere or other tools that build packages into their
# own DESTDIRs (one per package) and combine those packages
# with DESTDIR and all, the "leaking" of DESTDIR from one package
# to the next becomes an issue: the plugin directory for *libexample*
# should be `/usr/lib/example/plugins/` .. although this depends on
# the overall build environment and eventual use of the plugins.
#
# "Double destdir" refers to the situation where paths that contain
# a full DESTDIR (from one package) are used as paths in another package,
# and end up being interpreted relative to the latter package's DESTDIR.
# You end up with ${DESTDIR2}/${DESTDIR1}/usr/lib/example/plugins/
# which isn't the kind of path you want.
#
# This function strips a DESTDIR prefix from a variable that is
# assumed to hold a path, but only when it does indeed have
# that prefix and only if the result is still a path.  The
# variable is assumed to be an absolute path and not a link.
#
#
# Usage:
#    `stripdestdir (VAR DESTDIR_VALUE)`
#
# `VAR` names a variable (in the calling scope) and `DESTDIR_VALUE` is
# a string (the DESTDIR to strip from the beginning of the value of `VAR`).
#
# Examples:
#  - The resulting value of VAR is "/tree/yz" after each of these calls,
#    assuming the initial value of VAR as indicated:
#    VAR="/tree/x/tree/yz" DESTDIR_VALUE="/tree/x"
#    VAR="/tree/x/tree/yz" DESTDIR_VALUE="/tree/x/"
#    VAR="/tree/yz"        DESTDIR_VALUE="/tree/x"
#    VAR="/tree/yz"        DESTDIR_VALUE="/tree/x"
#    VAR="/tree/yz"        DESTDIR_VALUE="/tree/yzx"
#    VAR="/tree/yz"        DESTDIR_VALUE="/tree/y"
#

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2021, Rick van Rein <rick@openfortress.nl>
#

function(stripdestdir VAR DESTDIR_VALUE)
    if(IS_ABSOLUTE "${DESTDIR_VALUE}")
        # Ensure _DESTNEW ends with exactly one /
        set(_DESTNEW "${DESTDIR_VALUE}/")
        string(REGEX REPLACE "//$" "/" _DESTNEW "${_DESTNEW}")

        string(FIND ${${VAR}} ${_DESTNEW} _pos)
        if(_pos EQUAL 0)
            # Then the _DESTNEW was found at the start of the string; it
            # ends with a / so we can chop it off (and then restore the /
            # when setting the value in parent scope).
            string(LENGTH "${_DESTNEW}" _DESTLEN)
            string(SUBSTRING "${${VAR}}" ${_DESTLEN} -1 _VARNEW)
            set(${VAR} "/${_VARNEW}" PARENT_SCOPE)
        endif()
    endif()
endfunction(stripdestdir)
