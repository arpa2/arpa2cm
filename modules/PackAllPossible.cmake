# PackAllPossible.cmake -- find what target package formats can be made locally
#
# This package creates package formats for a maximum number of target formats:
#  - .tar.bz2 for source code
#  - .deb for Debian / Ubuntu / Mint Linux
#  - .rpm for RedHat / Fedora / SuSe Linux
#  - .pkg for Mac OS X
#
# The main output from this script is setting up the CPACK_GENERATOR list.
#
# The code below detects the available options automatically, by looking at
# their CPack generator support and the availability of the main driving
# program.  There are no warnings for missing out on options, so you simply
# may have to add such drivers if you intend to package for a wider range
# of target platforms.
#
# No notion is taken of machine formats, also because CMake might be used for
# cross-compiling.  Please do not forget to think for yourself.  Sure you can
# build a Windows package filled with Linux executables, but it is not going
# to be as useful as building a FreeBSD package with Linux executables.
#

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2017, Rick van Rein <rick@openfortress.nl>
#

# Always produce a source tar ball
set(CPACK_GENERATOR TGZ;TBZ2)

# Support DEB packaging for Debian / Ubuntu / Mint Linux
find_program(PROGRAM_LINUX_DEBBUILD dpkg-buildpackage)
if(PROGRAM_LINUX_DEBBUILD)
    list(APPEND CPACK_GENERATOR DEB)
endif()
unset(PROGRAM_LINUX_DEBBUILD CACHE)

# Support RPM packaging for RedHat / Fedora / SuSe Linux
find_program(PROGRAM_LINUX_RPMBUILD rpmbuild)
if(PROGRAM_LINUX_RPMBUILD)
    list(APPEND CPACK_GENERATOR RPM)
endif()
unset(PROGRAM_LINUX_RPMBUILD CACHE)

# Support PackageMaker packaging for Mac OS X
find_program(PROGRAM_MACOSX_PKGBUILD pkgbuild)
if(PROGRAM_MACOSX_PKGBUILD)
    list(APPEND CPACK_GENERATOR PackageMaker)
endif()
unset(PROGRAM_MACOSX_PKGBUILD CACHE)

# Support NSIS packaging for Windows
find_program(PROGRAM_WINDOWS_NSISBUILD makensis)
if(PROGRAM_WINDOWS_NSISBUILD)
    list(APPEND CPACK_GENERATOR NSIS)
endif()
unset(PROGRAM_WINDOWS_NSISBUILD CACHE)

find_program(PROGRAM_FREEBSD_PKG pkg)
if(PROGRAM_FREEBSD_PKG AND CMAKE_SYSTEM MATCHES "FreeBSD")
    list(APPEND CPACK_GENERATOR FREEBSD)
endif()
unset(PROGRAM_FREEBSD_PKG)

# Now check which of the listed generators are supported by this CPack
execute_process(COMMAND cpack --help OUTPUT_VARIABLE _cpack_help)
string(REGEX REPLACE ";" "\\\\;" _cpack_help "${_cpack_help}")
string(REGEX REPLACE "\n" ";" _cpack_help "${_cpack_help}")
set(_found_generators FALSE)
set(_supported_generators "")
foreach(line ${_cpack_help})
    if(line MATCHES "Generators")
        set(_found_generators TRUE)
    endif()
    if(_found_generators AND line MATCHES " = ")
        string(REGEX REPLACE " = .*" "" line "${line}")
        string(REPLACE " " "" line "${line}")
        list(APPEND _supported_generators "${line}")
    endif()
endforeach()
unset(_found_generators)

# Intersect the ones we might want, with the ones CPack supports
set(_desired_generators "${CPACK_GENERATOR}")
set(CPACK_GENERATOR "")
foreach(g ${_desired_generators})
    list(FIND _supported_generators "${g}" _has_g)
    if(_has_g GREATER -1)
        list(APPEND CPACK_GENERATOR "${g}")
    endif()
endforeach()
unset(_desired_generators)
unset(_supported_generators)

# Publish the results in the global cached scope
set(CPACK_GENERATOR "${CPACK_GENERATOR}"
    CACHE STRING "CPack generators that will be run"
)
