# UseInstallRPATH.cmake -- Convenience module that sets INSTALL_RPATH
# if that would be needed, given the setting for INSTALL_PREFIX.
#
# This module is largely cribbed from the CMake wiki,
#   https://gitlab.kitware.com/cmake/community/-/wikis/doc/cmake/RPATH-handling

#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Adriaan de Groot <groot@kde.org>
#

# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# the RPATH to be used when installing, but only if it's not a system directory
list(
    FIND
    CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES
    "${CMAKE_INSTALL_PREFIX}/lib"
    _arpa2_isSystemDir
)
if("${_arpa2_isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif()
unset(_arpa2_isSystemDir)
